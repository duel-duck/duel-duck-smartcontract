import * as anchor from "@coral-xyz/anchor";
import { Program, Wallet, web3, workspace } from "@coral-xyz/anchor";
import { Keypair, PublicKey } from "@solana/web3.js";
import {
  createMint,
  createAssociatedTokenAccount,
  getOrCreateAssociatedTokenAccount,
  mintTo,
  TOKEN_PROGRAM_ID,
  createTransferInstruction,
} from "@solana/spl-token";

describe("first", () => {
  const keypair1 = pg.wallet.keypair;

  const mint = new PublicKey("EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v");
  const fromAta = new PublicKey("8wi3kaDrqXkE6WEzCyy8ySh5JuQoBHv4gNscDYdzLedZ");
  const toAta = new PublicKey("8wi3kaDrqXkE6WEzCyy8ySh5JuQoBHv4gNscDYdzLedZ");

  it("Is initialized!", async () => {
      const pda_nr = 1;
      let answer = 1;
      let theme = "Some random theme";
      let description = "Huh?";
      let percent = 5;
      let bet = 1000000;
      let end = 1752221823;

      const pdaNrBuffer = Buffer.alloc(4);
      pdaNrBuffer.writeUInt32LE(pda_nr, 0);

      const [da_pda, _bump] = web3.PublicKey.findProgramAddressSync(
        [Buffer.from("data"), pdaNrBuffer],
        pg.PROGRAM_ID
      );
      console.log("data pda:" + da_pda.toBase58());

      //let addr = await getOrCreateAssociatedTokenAccount(pg.connection, keypair1, mint, keypair1.publicKey);
      //console.log(addr.address.toBase58());



    
    //await initialize(pda_nr, da_pda, answer, theme, description, percent,  bet, end);
    //await join(pda_nr, da_pda, answer);
    //await show(pda_nr, da_pda);
    //await reallocateMemory(pda_nr, da_pda);
    //await rewards(da_pda, 0);
    //await closeRoom(pda_nr, da_pda);


    async function initialize(pda_nr, da_pda, answer, theme, description, percent, bet, end) {
      const assAccInit = await getOrCreateAssociatedTokenAccount(pg.connection, keypair1, mint, keypair1.publicKey);
      console.log(assAccInit.address.toBase58());
      const txHashInit = await pg.program.methods
        .init(
          answer,
          theme,
          description,
          percent,
          bet,
          end,
          pda_nr
        )
        .accounts({
          dataAccount: da_pda,
          systemProgram: web3.SystemProgram.programId,
          user: keypair1.publicKey,
          fromAta: assAccInit.address.toBase58(),
          toAta: toAta,
          tokenProgram: TOKEN_PROGRAM_ID,
        })
        .signers([keypair1])
        .rpc();
      console.log(`https://explorer.solana.com/tx/${txHashInit}?cluster=devnet`);
    };

    async function join(pda_nr, da_pda, answer) {
      const assAccJoin = await getOrCreateAssociatedTokenAccount(pg.connection, keypair1, mint, keypair1.publicKey);
      console.log(assAccJoin.address.toBase58());
      const txHashJoin = await pg.program.methods
        .join(answer, pda_nr)
        .accounts({
          dataAccount: da_pda,
          systemProgram: web3.SystemProgram.programId,
          user: keypair1.publicKey,
          fromAta: assAccJoin.address.toBase58(),
          toAta: toAta,
          tokenProgram: TOKEN_PROGRAM_ID,
        })
        .signers([keypair1])
        .rpc();
      console.log(`https://explorer.solana.com/tx/${txHashJoin}?cluster=devnet`);
    };

    async function show(pda_nr, da_pda) {
      const txHash = await pg.program.methods
      .show(pda_nr)
      .accounts({
        dataAccount: da_pda,
        systemProgram: web3.SystemProgram.programId,
        user: keypair1.publicKey,
      })
      .signers([keypair1])
      .rpc();
      console.log(`https://explorer.solana.com/tx/${txHash}?cluster=devnet`);
    };

    async function closeRoom(pda_nr, da_pda) {
      const txHash = await pg.program.methods
      .closeRoom(pda_nr)
      .accounts({
        dataAccount: da_pda,
        user: keypair1.publicKey,
      })
      .signers([keypair1])
      .rpc();
      console.log(`https://explorer.solana.com/tx/${txHash}?cluster=devnet`);
    };

    async function rewards(da_pda, correctAnswer) {
      const Account = await pg.program.account.accountStruct.fetch(da_pda);

      let yesCounter = 0;
      let noCounter = 0;

      let walletsTo = [];
      let allWallets = [];
      let allParticipants = new Map();

      for (const user of Account.users) {
        const accOwner = user.user.toBase58();
        const accOwnerPC = new PublicKey(accOwner);
        console.log("Address: ", accOwnerPC.toBase58());
        if (user.answer == correctAnswer ) {
          walletsTo.push(accOwnerPC.toBase58());
        }

        allWallets.push(accOwnerPC.toBase58());
        allParticipants.set(accOwnerPC.toBase58(), user.answer);

        if (user.answer == 1) {
          yesCounter += 1;
        }

        if (user.answer == 0) {
          noCounter += 1;
        }
      }

      const pool = Account.users.length * Account.bet;
      let percentValue = Math.floor((pool / 100) * Account.percent);
      let finalPool = pool - percentValue;
      let finalReward = Math.floor(finalPool / walletsTo.length);


      const getBatchInstructions = async (batch) => {
        const instructions = await Promise.all(batch.map(async addressTo => {
          let acc = await getOrCreateAssociatedTokenAccount(pg.connection, keypair1, mint, new PublicKey(addressTo))
          return createTransferInstruction(
            fromAta,
            acc.address,
            keypair1.publicKey,
            finalReward
          );
        }));

        if (percentValue > 0) {
          const cashback = Math.floor(percentValue / 2);
          if (cashback > 0) {
            const acc = await getOrCreateAssociatedTokenAccount(
              pg.connection, keypair1, mint, new PublicKey(Account.author.toBase58())
            )
            instructions.push(createTransferInstruction(
              fromAta,
              acc.address,
              keypair1.publicKey,
              cashback,
            ));
          }
        }


        return instructions;
      };

      const batches = [];
      const batchSize = 1232;

      if (yesCounter == 0 || noCounter == 0) {
        percentValue = 0;
        walletsTo = allWallets;
        finalPool = pool;
        finalReward = Math.floor(finalPool / walletsTo.length);
      }


      for (let i = 0; i < walletsTo.length; i += batchSize) {
        const batch = walletsTo.slice(i, i + batchSize);
        const instructions = await getBatchInstructions(batch);
        batches.push(instructions);
      }

      for (const batch of batches) {
        const transaction = new web3.Transaction().add(...batch);
        transaction.feePayer = keypair1.publicKey;
        transaction.recentBlockhash = (await pg.connection.getLatestBlockhash()).blockhash;

        const signedTransaction = await pg.wallet.signTransaction(transaction);
        const txid = await pg.connection.sendRawTransaction(signedTransaction.serialize());
        console.log(`https://explorer.solana.com/tx/${txid}?cluster=devnet`);
      }
    };
  });
});
