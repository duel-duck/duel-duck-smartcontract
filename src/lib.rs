use anchor_lang::prelude::*;
use anchor_spl::token::{self, Token, TokenAccount, Transfer as SplTransfer};
use spl_associated_token_account::get_associated_token_address;
//use solana_program::system_instruction;
//use std::mem;

declare_id!("7iMofsAdkzcc7RQaG13XXbHwMA41VwY3c7UQuKd9RBhU");

#[program]
pub mod first {
    use super::*;

    pub fn init(
        ctx: Context<Initialization>,
        answer: u8,
        theme: String,
        description: String,
        percent: u32,
        bet: u32,
        end: u32,
        pda_nr: u32,
    ) -> Result<()> {
        if ctx.accounts.data_account.author != solana_program::pubkey!("11111111111111111111111111111111") {
            return Err(ProgramError::AccountAlreadyInitialized.into());
        }

        if percent > 20 {
            return Err(CustomError::InvalidPercent.into());
        }

        if bet < 1000000 {
           return Err(CustomError::SmallBet.into());
        }

        if bet % 1000000 != 0  {
           return Err(CustomError::InvalidBetAmount.into());
        }

        if ctx.accounts.to_ata.key().clone() != solana_program::pubkey!("8wi3kaDrqXkE6WEzCyy8ySh5JuQoBHv4gNscDYdzLedZ") {
          return Err(CustomError::InvalidPoolAddress.into());
        }

        let ata = get_associated_token_address(&ctx.accounts.user.key().clone(), &solana_program::pubkey!("EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v"));
        msg!("Associated Token Account: {}", ata);

        if ata.key().clone() != ctx.accounts.from_ata.key().clone() {
            return Err(CustomError::InvalidATA.into());
        }

        if answer != 0 && answer != 1 {
            return Err(CustomError::InvalidAnswerValue.into());
        }

        if theme.as_bytes().len() > 100 {
            return Err(CustomError::ThemeTooLong.into());
        }

        if description.as_bytes().len() > 600 {
            return Err(CustomError::DescriptionTooLong.into());
        }

        ctx.accounts.data_account.author = ctx.accounts.user.key();
        ctx.accounts.data_account.theme = theme.clone();
        ctx.accounts.data_account.description = description.clone();
        ctx.accounts.data_account.bet = bet.clone();
        ctx.accounts.data_account.percent = percent.clone();
        ctx.accounts.data_account.end = end.clone();
        ctx.accounts.data_account.users = Vec::new();

        let destination = &ctx.accounts.to_ata;
        let source = &ctx.accounts.from_ata;
        let token_program = &ctx.accounts.token_program;
        let authority = &ctx.accounts.user;

        ctx.accounts.data_account.users.push(Participant {
            user: ctx.accounts.user.key(),
            answer,
        });

        // Transfer tokens from taker to initializer
        let cpi_accounts = SplTransfer {
            from: source.to_account_info().clone(),
            to: destination.to_account_info().clone(),
            authority: authority.to_account_info().clone(),
        };
        let cpi_program = token_program.to_account_info();

        token::transfer(
            CpiContext::new(cpi_program, cpi_accounts),
            bet.clone().into(),
        )?;

        let seeds: &[&[u8]] = &[b"data", &pda_nr.to_le_bytes()];
        let (pda, _bump_seed) = Pubkey::find_program_address(seeds, &ctx.program_id);


        msg!("Room created!");
        msg!("Current executing program address: {}", ctx.program_id);
        msg!("Current room address: {}", pda.key());
        msg!("Your address: {}", ctx.accounts.data_account.author);
        msg!("Your token ata: {}", source.key());
        msg!("Pool address: {}", destination.key());


        msg!("Author: {}", ctx.accounts.data_account.author);
        msg!("Theme: {}!", theme);
        msg!("Description: {}!", description);
        msg!("Bet: {} USDC!", bet.clone() / 1_000_000);
        msg!("Ending (unix): {}!", end);

        msg!("Transfer of {} tokens ended successfuly!", bet.clone());

        Ok(())
    }

    pub fn join(ctx: Context<Join>, answer: u8, pda_nr: u32) -> Result<()> {
        if ctx.accounts.data_account.author == solana_program::pubkey!("11111111111111111111111111111111") {
            return Err(ProgramError::UninitializedAccount.into());
        }

        for participant in ctx.accounts.data_account.users.iter() {
            if ctx.accounts.user.key() == participant.user {
                return Err(CustomError::AlreadyParticipant.into());
            }
        }

        if ctx.accounts.to_ata.key().clone() != solana_program::pubkey!("8wi3kaDrqXkE6WEzCyy8ySh5JuQoBHv4gNscDYdzLedZ") {
           return Err(CustomError::InvalidPoolAddress.into());
        }

        let ata = get_associated_token_address(&ctx.accounts.user.key().clone(), &solana_program::pubkey!("EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v"));
        msg!("Associated Token Account: {}", ata);

        if ata.key().clone() != ctx.accounts.from_ata.key().clone() {
            return Err(CustomError::InvalidATA.into());
        }

        if answer != 0 && answer != 1 {
            return Err(CustomError::InvalidAnswerValue.into());
        }

        let destination = &ctx.accounts.to_ata;
        let source = &ctx.accounts.from_ata;
        let token_program = &ctx.accounts.token_program;
        let authority = &ctx.accounts.user;

        ctx.accounts.data_account.users.push(Participant {
            user: ctx.accounts.user.key(),
            answer,
        });
        msg!("User {} joined room {}!", ctx.accounts.user.key(), pda_nr);

        // Transfer tokens from taker to initializer
        let cpi_accounts = SplTransfer {
            from: source.to_account_info().clone(),
            to: destination.to_account_info().clone(),
            authority: authority.to_account_info().clone(),
        };
        let cpi_program = token_program.to_account_info();

        token::transfer(
            CpiContext::new(cpi_program, cpi_accounts),
            ctx.accounts.data_account.bet.into(),
        )?;

        let seeds: &[&[u8]] = &[b"data", &pda_nr.to_le_bytes()];
        let (pda, _bump_seed) = Pubkey::find_program_address(seeds, &ctx.program_id);

        msg!("Current executing program address: {}", ctx.program_id);
        msg!("Current room address: {}", pda.key());
        msg!("Your address: {}", ctx.accounts.user.key());
        msg!("Your token ata: {}", source.key());
        msg!("Pool address: {}", destination.key());

        Ok(())
    }

    pub fn show(ctx: Context<Show>, pda_nr: u32) -> Result<()> {
        if ctx.accounts.data_account.author == solana_program::pubkey!("11111111111111111111111111111111") {
            return Err(ProgramError::UninitializedAccount.into());
        }

        msg!("Room: {}", pda_nr);
        msg!("Theme: {}", ctx.accounts.data_account.theme);
        msg!("Author: {}", ctx.accounts.data_account.author);
        msg!("Bet: {} USDC!", ctx.accounts.data_account.bet / 1_000_000);

        let seeds: &[&[u8]] = &[b"data", &pda_nr.to_le_bytes()];
        let (pda, _bump_seed) = Pubkey::find_program_address(seeds, &ctx.program_id);

        msg!("Current executing program address: {}", ctx.program_id);
        msg!("Current room address: {}", pda.key());

        msg!("Users:");
        for participant in ctx.accounts.data_account.users.iter() {
            msg!(
                "Address: {}, Answer: {}",
                participant.user,
                participant.answer
            );
        }
        Ok(())
    }

    pub fn close_room(ctx: Context<CloseAccounts>, pda_nr: u32) -> Result<()> {
        if *ctx.accounts.user.key
            != solana_program::pubkey!("okrvpL26GtSkpAbEBzi7QrBhjWHwGusHNXoJMdeBuqx")
        {
            return Err(ProgramError::InvalidArgument.into());
        }
        msg!(
            "Closing an account {}\nRoom: {}",
            ctx.accounts.data_account.key(),
            pda_nr
        );
        Ok(())
    }
}

#[error_code]
pub enum CustomError {
    #[msg("Invalid commission percent")]
    InvalidPercent,
    #[msg("Too small bet")]
    SmallBet,
    #[msg("Invalid bet amount")]
    InvalidBetAmount,
    #[msg("Invalid answer value")]
    InvalidAnswerValue,
    #[msg("Invalid pool address provided")]
    InvalidPoolAddress,
    #[msg("Invalid associated token account provided")]
    InvalidATA,
    #[msg("This address already in game")]
    AlreadyParticipant,
    #[msg("Description is too long")]
    DescriptionTooLong,
    #[msg("Theme is too long")]
    ThemeTooLong,
}

#[derive(Accounts)]
#[instruction(answer: u8, theme: String, description: String, percent: u32, bet: u32, end: u32, pda_nr: u32)]
pub struct Initialization<'info> {
    #[account(init_if_needed, payer = user, space = 8 + 40 + 9999, seeds = [b"data", &pda_nr.to_le_bytes()], bump)]
    pub data_account: Account<'info, AccountStruct>,
    #[account(mut)]
    pub user: Signer<'info>,
    #[account(mut)]
    pub from_ata: Account<'info, TokenAccount>,
    #[account(mut)]
    pub to_ata: Account<'info, TokenAccount>,
    pub system_program: Program<'info, System>,
    pub token_program: Program<'info, Token>,
}

#[derive(Accounts)]
#[instruction(answer: u8, pda_nr: u32)]
pub struct Join<'info> {
    #[account(init_if_needed, payer = user, space = 8 + 40 + 9999, seeds = [b"data", &pda_nr.to_le_bytes()], bump)]
    pub data_account: Account<'info, AccountStruct>,
    #[account(mut)]
    pub user: Signer<'info>,
    #[account(mut)]
    pub from_ata: Account<'info, TokenAccount>,
    #[account(mut)]
    pub to_ata: Account<'info, TokenAccount>,
    pub system_program: Program<'info, System>,
    pub token_program: Program<'info, Token>,
}

#[derive(Accounts)]
#[instruction(pda_nr: u32)]
pub struct Memory<'info> {
    #[account(
        seeds = [b"data", &pda_nr.to_le_bytes()],
        bump,
        mut,
        realloc = 8 + 40 + 20000,
        realloc::payer=user,
        realloc::zero=false)]
    pub data_account: Account<'info, AccountStruct>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(pda_nr: u32)]
pub struct Show<'info> {
    #[account(init_if_needed, payer = user, space = 8 + 40 + 9999, seeds = [b"data", &pda_nr.to_le_bytes()], bump)]
    pub data_account: Account<'info, AccountStruct>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(pda_nr: u32)]
pub struct ShowRealloc<'info> {
    #[account(init_if_needed, payer = user, space = 8 + 40 + 20000, seeds = [b"data", &pda_nr.to_le_bytes()], bump)]
    pub data_account: Account<'info, AccountStruct>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
#[instruction(pda_nr: u32)]
pub struct CloseAccounts<'info> {
    #[account(mut, close = user, seeds = [b"data", &pda_nr.to_le_bytes()], bump)]
    pub data_account: Account<'info, AccountStruct>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[zero_copy]
#[derive(AnchorSerialize, AnchorDeserialize)]
pub struct Participant {
    user: Pubkey,
    answer: u8,
}

#[account]
pub struct AccountStruct {
    author: Pubkey,
    theme: String,
    description: String,
    percent: u32,
    bet: u32,
    end: u32,
    users: Vec<Participant>,
}